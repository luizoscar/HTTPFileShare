#!/bin/bash
script_dir=$(readlink -f "${BASH_SOURCE[0]}" | xargs -d '\n' dirname )
files_dir="${script_dir}/fileshare"
app_path="${files_dir}/HttpFileShare.py"
bashrc_path="${HOME}/.bashrc"

# Locate python runtime
python_path=$(which python3)
[[ -z "${python_path}" ]] && python_path=$(which python)

#Show an error message and exit
do_show_error() {
    [[ -n "$1" ]] && echo -e "\e[31;1m$1\e[m\n"
    exit 1
}

# Validate the files
[[ -z "${python_path}" ]] &&  do_show_error "Error: Unable to locate the python runtime, please install python3 and try again"
[[ -d "${files_dir}" ]] || do_show_error "Error: The path to the application was not found: ${files_dir}"
[[ -f "${app_path}" ]] || do_show_error "Error: The application was not found: ${app_path}"
[[ -f "${files_dir}/first_words.txt" ]] || do_show_error "Error: The list of first words was not found: ${files_dir}/first_words.txt"
[[ -f "${files_dir}/second_words.txt" ]] || do_show_error "Error: The list of second words was not found: ${files_dir}/second_words.txt"
[[ -f "${bashrc_path}" ]] ||  do_show_error "Error: File not found: ${bashrc_path}"

#Remove the previous alias, if exists
grep -Fq "alias share=" "${bashrc_path}" && sed -i "/^alias share=/d" "${bashrc_path}"

#Add the new alias
echo "alias share='${python_path} ${app_path} -f'" >> "${bashrc_path}"

#Reload bashrc
source ~/.bashrc

# Call the app
echo "Install completed, you may now run 'share'."